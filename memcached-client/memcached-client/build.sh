#!/bin/bash
if [ $# -ne 1 ]; then
    echo -e "usage: ./build.sh target \n"
    exit 1
fi
target=$1
topdir=/home/guojian/mc_test/mcproxy
exe=
include_dir=./include
libevent_dir=/usr/local/libevent
src_proxy="src/proxy/mc_proxy.c"
src_client="src/client/mc_client.c"
src_other="
     src/mc/binary_op.c
     src/util/ini.c
     src/util/parse_ini_file.c
     src/util/conn.c
     src/util/socket.c
     src/mc/sendrecv.c
     "

if [ $target != "p" ] && [ $target != "c" ]; then
    echo -e "please specify target as either (p)roxy or (c)lient\n"
    exit 1
fi

src=
if [ $target == "p" ]; then
    exe=mcproxy
    src="${src_other} ${src_proxy}"
else
    exe=mcclient
    src="${src_other} ${src_client}"
fi

rm $exe
gcc -g  -I${include_dir} -I${libevent_dir}/include -L${libevent_dir}/lib -levent -g -pg -pthread -o $exe $src
echo -e "generate $exe => `ls $exe`"
