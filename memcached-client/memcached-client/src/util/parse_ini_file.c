#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <general.h>
#include "ini.h"

#define MATCH(s, n) (strcmp(section, s) == 0 && strcmp(name, n) == 0)

static int handler(void* user, const char* section, const char* name,
                   const char* value)
{
    server_config *pconfig = (server_config *)user;

    if (MATCH("mc_server_info", "servers")) {
        pconfig->mc_servers[pconfig->num_servers] = strdup(value);
        pconfig->num_servers++;
    } else if (MATCH("mc_server_info", "ports")) {
        pconfig->mc_server_ports[pconfig->num_server_ports] = atoi(value);
        pconfig->num_server_ports++;
    } else if (MATCH("mc_proxy_info", "proxys")) {
        pconfig->mc_proxys[pconfig->num_proxys]   = strdup(value);
        pconfig->num_proxys++;
    } else if (MATCH("mc_proxy_info", "ports")) {
        pconfig->mc_proxy_ports[pconfig->num_proxy_ports] = atoi(value);
        pconfig->num_proxy_ports++;
    } else if (MATCH("mc_client_info", "clients")) {
        pconfig->mc_clients[pconfig->num_clients] = strdup(value);
        pconfig->num_clients++;
    } else if (MATCH("mc_client_info", "conn_per_client")) {
        pconfig->num_conn_per_client = atoi(value);
    } else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}

int find_port_by_server(char *server) {
    int port = 0;
	int i=0;
    for ( i=0; i<s_config.num_servers; i++) {
        if (!strcmp(server, s_config.mc_servers[i])) {
            port = s_config.mc_server_ports[i];
            break;
        }
    }
    for ( i=0; i<s_config.num_proxys; i++) {
        if (!strcmp(server, s_config.mc_proxys[i])) {
            port = s_config.mc_proxy_ports[i];
            break;
        }
    }
    return port;
}

int parse_ini_file(char *file_name)
{
    //configuration config;
    s_config.num_server_ports = 0;
    s_config.num_proxy_ports  = 0;
    s_config.num_servers      = 0;
    s_config.num_proxys       = 0;
    s_config.num_clients      = 0;
    s_config.num_conn_per_client = 0;

    if (ini_parse(file_name, handler, &s_config) < 0) {
        printf("Can't load %s\n", file_name);
        return 1;
    }
    printf("Config loaded from %s: num(server=%d s_port=%d proxy=%d p_port=%d client=%d num_conn=%d)\n",
           file_name,
           s_config.num_servers, s_config.num_server_ports,
           s_config.num_proxys, s_config.num_proxy_ports,
           s_config.num_clients, s_config.num_conn_per_client);

    if (s_config.num_proxy_ports != s_config.num_proxys ||
            s_config.num_server_ports != s_config.num_servers) {
        fprintf(stderr, "mc config file error\n");
        exit(EXIT_FAILURE);
    }
	int i=0;
    for ( i=0; i<s_config.num_proxys; i++) {
        fprintf(stderr, "proxy[%d]=%s:%d\n", i, s_config.mc_proxys[i], s_config.mc_proxy_ports[i]);
    }

    for (i=0; i<s_config.num_servers; i++) {
        fprintf(stderr, "server[%d]=%s:%d\n", i, s_config.mc_servers[i], s_config.mc_server_ports[i]);
    }
    return 0;
}

//int main(int argc, char* argv[]) {
int do_parse() {
    char *f_name = "/home/guojian/mc_test/mcproxy/mcconfig.ini";
    // file = fopen(f_name, "r");
    // if (NULL == file) {
    //     fprintf(stderr, " => no such file");
    //     return -1;
    // }
    parse_ini_file(f_name);
    return 0;
}

