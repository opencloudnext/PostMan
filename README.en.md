# PostMan

#### Description
PostMan is an alternative approach to rapidly mitigate load imbalance for services processing small requests. Motivated by the observation that processing large packets incurs far less CPU overhead than processing
small ones, PostMan deploys a number of middleboxes called helpers to assemble small packets into large ones for the heavily-loaded server.
