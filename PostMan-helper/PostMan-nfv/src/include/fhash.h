#ifndef __FHASH_H_
#define __FHASH_H_

#include <sys/queue.h>

#include <rte_hash.h>
#ifdef RTE_MACHINE_CPUFLAG_SSE4_2
#include <rte_hash_crc.h>
#endif
#include "tcp_stream.h"

#define NUM_BINS (131072)     /* 132 K entries per thread*/
#define NUM_BINS_FLOWS 		(131072)     /* 132 K entries per thread*/
#define NUM_BINS_LISTENERS	(1024)	     /* assuming that chaining won't happen excessively */
#define TCP_AR_CNT 		(3)


#define STATIC_TABLE FALSE

typedef struct hash_bucket_head {
	tcp_stream *tqh_first;
	tcp_stream **tqh_last;
} hash_bucket_head;

typedef struct list_bucket_head {
	struct tcp_listener *tqh_first;
	struct tcp_listener **tqh_last;
} list_bucket_head;
/* hashtable structure */
struct hashtable {
	uint8_t ht_count ;                    // count for # entry
	
		uint32_t bins;

/*	union {
		hash_bucket_head *ht_table;
		list_bucket_head *lt_table;
	};*/

#if STATIC_TABLE
	tcp_stream* ht_array[NUM_BINS][TCP_AR_CNT];
#endif
	hash_bucket_head ht_table[NUM_BINS];

	// functions
	unsigned int (*hashfn) (const tcp_stream *);
	int (*eqfn) (const tcp_stream *, const tcp_stream *);
};

/*functions for hashtable*/
struct hashtable *CreateHashtable(unsigned int (*hashfn) (const tcp_stream*), 
								   int (*eqfn) (const tcp_stream*, 
												const tcp_stream *));
void DestroyHashtable(struct hashtable *ht);


int HTInsert(struct hashtable *ht, tcp_stream *);
void* HTRemove(struct hashtable *ht, tcp_stream *);
tcp_stream* HTSearch(struct hashtable *ht, const tcp_stream *);

#endif /* __FHASH_H_ */
