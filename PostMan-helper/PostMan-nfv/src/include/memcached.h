/*
 * Copyright (c) <2008>, Sun Microsystems, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the  nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY SUN MICROSYSTEMS, INC. ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SUN MICROSYSTEMS, INC. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*
 * Summary: Constants used by to implement the binary protocol.
 *
 * Copy: See Copyright for the status of this software.
 *
 * Author: Trond Norbye <trond.norbye@sun.com>
 */

#ifndef __MEMCACHED_H_
#define __MEMCACHED_H_

    /**
     * Definition of the legal "magic" values used in a packet.
     * See section 3.1 Magic byte
     */
typedef enum _header_type {
	S_REQUEST  = 0x60,
	M_REQUEST = 0x61,
	S_RESPONSE = 0x10,
	M_RESPONSE = 0x11,
    CONNECT = 0x40,
    SLAVE_CONN = 0x41, // CONNECT from nfv
    LEADER_CONN = 0x42 // CONNECT from leader
} header_type;



/**
 * Definition of the header structure for a request packet.
 * See section 2
 */
typedef struct _msg_header {
		uint8_t type;
		uint8_t src_ip;
		uint16_t port;
		uint16_t num_msg;
		uint16_t len_msg;
		uint32_t ack;
} msg_header;

typedef struct _connect_header {
		uint8_t type;
		uint32_t src_ip;
		uint16_t port;
		uint16_t num_msg;
		uint16_t len_msg;
		//uint32_t ack;
} connect_header;


#endif /* PROTOCOL_BINARY_H */
