# PostMan Project Governance
This document describes the governance of PostMan team.

## Maintainership

There are different types of maintainers, with different responsibilities, but
all maintainers have 3 things in common:

1. They share responsibility in the success of PostMan.
2. They have made a long-term, recurring time investment to improve the project.
3. They spend that time doing whatever needs to be done, not necessarily what
is the most interesting or fun.

## Reviewers

A reviewer is a core maintainer within the project.
They share in reviewing issues & pull requests and merge a code change into 
the project.

Reviewers are part of the PostMan but do not have write access.
Becoming a reviewer is a core aspect in the journey to becoming a committer.

## Committers

A committer is a core maintainer who is responsible for the overall quality and
stewardship of the project. They share the same reviewing responsibilities as
reviewers, but are also responsible for upholding the project bylaws as well as
participating in project level votes.

Committers are part of the organization with write access to all repositories.
Committers are expected to remain actively involved in the project and
participate in voting and discussing of proposed project level changes.

## How are decisions made?

All decisions follow the same 3 steps:

* Step 1: Open a pull request. Anyone can do this.

* Step 2: Discuss the pull request. Anyone can do this.

* Step 3: Merge or refuse the pull request. Who does this depends on the nature
of the pull request and which areas of the project it affects.

