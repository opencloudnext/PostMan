# PostMan
[![](mulan_badge.jpg)](https://portal.mulanos.cn//)

---

## Description
PostMan is an alternative approach to rapidly mitigate load imbalance for services processing small requests. Motivated by the observation that processing large packets incurs far less CPU overhead than processing
small ones, PostMan deploys a number of middleboxes called helpers to assemble small packets into large ones for the heavily-loaded server.

If you are interested in PostMan, a detailed description can be got in our [ATC 2019][atc2019] paper.

[atc2019]: https://fangmingliu.github.io/files/Postman-ATC19-FangmingLiu.pdf


## Using PostMan
1. Data migration in Memecached
- Branch: master
- Server: memcached-server
- Client: Message-benchmark
