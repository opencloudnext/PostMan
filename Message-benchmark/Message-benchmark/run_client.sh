#!/bin/bash

#./run [begin PORT] [port num] [conn/port] [ip] [loop times] [len_msg] [num_msg]

#echo for example: ./run_N_client.sh 4555 10 10 127.0.0.1 1000 1000 9 cepser
if (($3 > 8))
then
	((loop=$[$5/$3*6]))
else
	((loop=$5))
fi
echo "loop times: ${loop}"
for((i=0; i<$2; i++)) # for every port
do
	(( cur_port=$1 + $i))
	for (( x=0; x<$3; x++ )) # run $3 threads
	do
		#time /public/msg_benchmark/client ${cur_port} $4 $5 $6 $7 &
		#time ./$8 ${cur_port} $4 $5 $6 $7 &
		#/public/msg_benchmark/client ${cur_port} $4 $loop $6 $7 &
		./epoll_client ${cur_port} $4 $loop $6 $7 &
		#echo /public/msg_benchmark/client ${cur_port} $4 $loop $6 $7 &
	done
done
