#!/bin/bash

#  [server num] [start port] [run name]
echo > pid.txt
p="/public/msg_benchmark/trace/"
for(( i=0; i<$1; i++ ))
do
	((c=$i+$2))
	#/public/msg_benchmark/$3 $c rps_${c} len_${c}.txt  &
	#/public/msg_benchmark/server $c rps_${c} len_${c}.txt  &
	/public/msg_benchmark/server $c ${p} &
	echo $c ${p}rps_${c}.txt ${p}len_${c}.txt
	echo $! >> pid.txt
done

#kill -SIGINT `ps -aux | grep $0| awk -F' ' '{print $2}'`
