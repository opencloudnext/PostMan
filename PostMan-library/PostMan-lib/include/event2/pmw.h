/*************************************************************************
    > File Name: hpmw.h
    > Author: wtao
    > Mail: tao.wang0221@gmail.com 
    > Created Time: Tue 06 Sep 2016 05:35:49 PM CST
 ************************************************************************/

#ifndef _H_PMW_
#define _H_PMW_

#ifdef __cplusplus
extern "C" {
#endif


#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <assert.h>


int pm_socket(int domain, int type, int protocol);

int pm_epoll_wait(int epfd, struct epoll_event *events, 
		int maxevents, int timeout);

int pm_epoll_ctl(int epfd, int op, int fd, struct epoll_event *ev);

int pm_bind(int sockfd, struct sockaddr *addr, socklen_t addrlen);
int pm_listen(int sockfd, int backlog);
int pm_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);

int pm_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
ssize_t pm_write(int fd, const char *buf, size_t count);
ssize_t pm_writev(int fd, const struct iovec *iov, int iovcnt);
ssize_t pm_read(int fd, char *buf, size_t count);
ssize_t pm_readv(int fd, const struct iovec *iov, int iovcnt);
int pm_fcntl(int fd, int cmd, long arg);

int pm_pipe(int pipefd[2]);

// Tao
ssize_t pm_sendmsg(int fd, const struct msghdr *msg, int flags);

int pm_getsockopt(int sockfd, int level, int optname, void *optval, socklen_t *optlen);


int pm_setsockopt(int sockfd, int level, int optname, const void *optval, socklen_t optlen);
#ifdef __cplusplus
}
#endif

#endif
