TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += \
	include \

SOURCES += \
    arc4random.c \
    buffer_iocp.c \
    buffer.c \
    bufferevent_async.c \
    bufferevent_filter.c \
    bufferevent_openssl.c \
    bufferevent_pair.c \
    bufferevent_ratelim.c \
    bufferevent_sock.c \
    bufferevent.c \
    devpoll.c \
    epoll_sub.c \
    epoll.c \
    evdns.c \
    event_iocp.c \
    event_tagging.c \
    event.c \
    evmap.c \
    evrpc.c \
    evport.c \
    evthread_pthread.c \
    evthread_win32.c \
    evthread.c \
    evutil.c \
    evutil_rand.c \
    http.c \
    kqueue.c \
    listener.c \
    log.c \
    pmw-server.c \
    poll.c \
    select.c \
    signal.c \
    strlcpy.c \
    win32select.c \
    sample/dns-example.c \
    sample/event-test.c \
    sample/hello-world.c \
    sample/http-server.c \
    sample/le-proxy.c \
    sample/signal-test.c \
    sample/time-test.c \
    test/bench.c \
    test/bench_cascade.c \
    test/bench_http.c \
    test/bench_httpclient.c \
    test/regress.c \
    test/regress.gen.c \
    test/regress_buffer.c \
    test/regress_bufferevent.c \
    test/regress_dns.c \
    test/regress_et.c \
    test/regress_http.c \
    test/regress_iocp.c \
    test/regress_listener.c \
    test/regress_main.c \
    test/regress_minheap.c \
    test/regress_rpc.c \
    test/regress_ssl.c \
    test/regress_testutils.c \
    test/regress_thread.c \
    test/regress_util.c \
    test/regress_zlib.c \
    test/test-changelist.c \
    test/test-eof.c \
    test/test-init.c \
    test/test-ratelim.c \
    test/test-time.c \
    test/test-weof.c \
    test/tinytest.c

HEADERS += \
    compat/sys/queue.h \
    include/event2/buffer.h \
    include/event2/buffer_compat.h \
    include/event2/bufferevent.h \
    include/event2/bufferevent_compat.h \
    include/event2/bufferevent_ssl.h \
    include/event2/bufferevent_struct.h \
    include/event2/dns.h \
    include/event2/dns_compat.h \
    include/event2/dns_struct.h \
    include/event2/event.h \
    include/event2/event_compat.h \
    include/event2/event_struct.h \
    include/event2/http.h \
    include/event2/http_compat.h \
    include/event2/http_struct.h \
    include/event2/keyvalq_struct.h \
    include/event2/listener.h \
    include/event2/pmw.h \
    include/event2/rpc.h \
    include/event2/rpc_compat.h \
    include/event2/rpc_struct.h \
    include/event2/tag.h \
    include/event2/tag_compat.h \
    include/event2/thread.h \
    include/event2/util.h \
    test/regress.gen.h \
    test/regress.h \
    test/regress_testutils.h \
    test/tinytest.h \
    test/tinytest_local.h \
    test/tinytest_macros.h \
    WIN32-Code/event2/event-config.h \
    WIN32-Code/tree.h \
    bufferevent-internal.h \
    changelist-internal.h \
    configure.ac \
    defer-internal.h \
    evbuffer-internal.h \
    evdns.h \
    event-internal.h \
    event.h \
    evhttp.h \
    evmap-internal.h \
    evrpc-internal.h \
    evrpc.h \
    evsignal-internal.h \
    evthread-internal.h \
    evutil.h \
    ht-internal.h \
    http-internal.h \
    iocp-internal.h \
    ipv6-internal.h \
    log-internal.h \
    make-event-config.sed \
    minheap-internal.h \
    mm-internal.h \
    pmw-server.h \
    ratelim-internal.h \
    strlcpy-internal.h \
    util-internal.h

DISTFILES += \
